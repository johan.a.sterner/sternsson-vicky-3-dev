﻿COUNTRIES = {
	c:R20 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion #Multiculturalism not possible without human rights
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_magic

		set_tax_level = very_high
	}
}