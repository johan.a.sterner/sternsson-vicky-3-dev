﻿COUNTRIES = {
	c:L55 ?= {
		effect_starting_technology_tier_5_tech = yes
		add_technology_researched = tradition_of_equality
		
		effect_starting_politics_traditional = yes

		# Laws
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_in_the_workplace
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
	}
}