﻿COUNTRIES = {
	c:A25 ?= {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_laissez_faire # Gave this in place of free-trade
		activate_law = law_type:law_mercantilism # Not allowed free trade due to tech
		activate_law = law_type:law_per_capita_based_taxation 
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property #Not allowed women in workplace without voting
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned
	
	}
}