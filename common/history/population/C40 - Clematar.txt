﻿POPULATION = { #Taychendi
	c:C40 = { #Clematar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C41 = { #Scenkar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C42 = { #Urvand
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C43 = { #Enlarmai
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C44 = { #Varaendi
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C45 = { #Klerechend
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C46 = { #Nanru Nakar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C48 = { #Royakottar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C49 = { #Orenkoraim
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	#c:C51 = { #Chendhya
	#	
	#	effect_starting_pop_wealth_low = yes
	#	effect_starting_pop_literacy_baseline = yes
	#
	#}
	
	c:C52 = { #Advagar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C54 = { #Caergaraen
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C58 = { #Kattisangmar
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C62 = { #X
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C63 = { #Vettakaramai
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C64 = { #Uesrlarmai
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C65 = { #Pattivaram
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C66 = { #Calinkend
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C67 = { #Kannakirva
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C68 = { #Ethra
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
	
	c:C69 = { #Sarihaddu
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}
}