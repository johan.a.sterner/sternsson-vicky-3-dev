﻿not_a_tolerance_group = {
	NOR = {
		this = cannorian_tolerance
		this = cannorian_tolerance_plus
		this = bulwari_tolerance
		this = bulwari_tolerance_plus
		this = sarhali_tolerance
		this = sarhali_tolerance_plus
		this = rahen_tolerance
		this = rahen_tolerance_plus
		this = fp_tolerance
		this = fp_tolerance_plus
		this = gerudian_tolerance
		this = gerudian_tolerance_plus
		this = haless_tolerance
		this = haless_tolerance_plus
		this = aelantiri_tolerance
		this = aelantiri_tolerance_plus
		this = all_heritage_tolerance
		this = dwarovar_dwarf_tolerance
		this = dwarovar_dwarf_tolerance_plus
		this = dwarovar_goblin_tolerance
		this = dwarovar_goblin_tolerance_plus
		this = dwarovar_orc_tolerance
		this = dwarovar_orc_tolerance_plus
	}
}

is_centaur = {
    has_discrimination_trait = centaur_race_heritage
}

is_dwarven = {
    has_discrimination_trait = dwarven_race_heritage
}

culture_is_elven = {
	has_discrimination_trait = elven_race_heritage
}

character_is_elven = {
	culture = { culture_is_elven = yes }
}

is_gnollish = {
	has_discrimination_trait = gnollish_race_heritage
}

is_gnomish = {
	has_discrimination_trait = gnomish_race_heritage
}

is_goblin = {
	has_discrimination_trait = goblin_race_heritage
}

is_halfling = {
	has_discrimination_trait = halfling_race_heritage
}

is_half_elven = {
	AND = {
		OR = {
			culture_is_elven = yes
			is_ruinborn = yes
		}
		is_human = yes
	}
}

is_half_orcish = {
	AND = {
		is_orcish = yes
		is_human =yes
	}
}

is_harimari = {
	has_discrimination_trait = harimari_race_heritage
}

is_harpy = {
	has_discrimination_trait = harpy_race_heritage
}

is_hobgoblin = {
	has_discrimination_trait = hobgoblin_race_heritage
}

is_human = {
	OR = {
		has_discrimination_trait = cannorian_heritage
		has_discrimination_trait = bulwari_heritage
		has_discrimination_trait = west_sarhaly_heritage
		has_discrimination_trait = east_sarhaly_heritage
		has_discrimination_trait = south_sarhaly_heritage
		has_discrimination_trait = raheni_heritage
		has_discrimination_trait = northeast_halessi_heritage
		has_discrimination_trait = southeast_halessi_heritage
		has_discrimination_trait = fp_heritage
	}
}

is_kobold = {
	has_discrimination_trait = kobold_race_heritage
}

is_lizardman = {
	has_discrimination_trait = lizardman_race_heritage
}

is_mechanim = {
	has_discrimination_trait = mechanim_race_heritage
}

is_ogre = {
	has_discrimination_trait = ogre_race_heritage
}

is_orcish = {
	has_discrimination_trait = orcish_race_heritage
}

is_ruinborn = {
	has_discrimination_trait = aelantiri_heritage
}

is_troll = {
    has_discrimination_trait = troll_race_heritage
}

is_degenerated_elf = {
	is_ruinborn = yes
	has_discrimination_trait = degenerated_elf
}

is_same_race_as_any_primary_culture = {
	custom_description = {
		text = is_same_race_as_any_primary_culture_tt
		# List every race here
		OR = {
			AND = {
				is_centaur = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_centaur = yes
					}
				}
			}
			AND = {
				is_dwarven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_dwarven = yes
					}
				}
			}
			AND = {
				culture_is_elven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						culture_is_elven = yes
					}
				}
			}
			AND = {
				is_gnollish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_gnollish = yes
					}
				}
			}
			AND = {
				is_gnomish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_gnomish = yes
					}
				}
			}
			AND = {
				is_goblin = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_goblin = yes
					}
				}
			}
			AND = {
				is_halfling = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_halfling = yes
					}
				}
			}
			AND = {
				is_half_elven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_half_elven = yes
					}
				}
			}
			AND = {
				is_half_orcish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_half_orcish = yes
					}
				}
			}
			AND = {
				is_harimari = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_harimari = yes
					}
				}
			}
			AND = {
				is_harpy = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_harpy = yes
					}
				}
			}
			AND = {
				is_hobgoblin = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_hobgoblin = yes
					}
				}
			}
			AND = {
				is_human = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_human = yes
					}
				}
			}
			AND = {
				is_kobold = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_kobold = yes
					}
				}
			}
			AND = {
				is_ogre = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_ogre = yes
					}
				}
			}
			AND = {
				is_orcish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_orcish = yes
					}
				}
			}
			AND = {
				is_ruinborn = yes
				# Special stuff for Ruinborn so we don't need to make race for each of them
				shares_heritage_trait_with_any_primary_culture = $COUNTRY$
			}
			AND = {
				is_lizardman = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_lizardman = yes
					}
				}
			}
			AND = {
				is_troll = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_troll = yes
					}
				}
			}
			AND = {
				is_troll = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_troll = yes
					}
				}
			}
		}
	}
}

is_goblinoid_group = {
	custom_description = {
		text = is_goblinoid_group_tt
		OR = {
			is_goblin = yes
			is_hobgoblin = yes
		}
	}
}

is_ruinborn_group = {
	custom_description = {
		text = is_ruinborn_group_tt
		is_ruinborn = yes
	}
}

is_giantkin_group = {
	custom_description = {
		text = is_giantkin_group_tt
		OR = {
			is_troll = yes
			is_ogre = yes
		}
	}
}

is_monstrous_culture = {
	custom_description = {
		text = is_monstrous_race_tt
		OR = {
			is_centaur = yes
			is_kobold = yes
			is_gnollish = yes
			is_goblin = yes
			is_harpy = yes
			is_hobgoblin = yes
			is_lizardman = yes
			is_ogre = yes
			is_orcish = yes
			is_half_orcish = yes
			is_troll = yes
			is_degenerated_elf = yes
		}
	}
}

is_non_monstrous_culture = {
	custom_description = {
		text = is_non_monstrous_race_tt
		NOT = { is_monstrous_culture = yes }
	}
}

pop_race_accepted = {
	custom_description = {
		text = race_is_accepted_by_racial_laws_tt
		culture = {
			cultural_race_accepted = {
				COUNTRY = prev.$COUNTRY$
			}
		}
	}
}

cultural_race_accepted = {
	custom_description = {
		text = race_is_accepted_by_racial_laws_tt
		OR = {
			AND = {
				$COUNTRY$ = { has_law = law_type:law_same_race_only }
				is_same_race_as_any_primary_culture = { # Always accept your own race
					COUNTRY = $COUNTRY$
				}
			}
			
			# Monstrous vs non-monstrous
			AND = {
				$COUNTRY$ = { has_law = law_type:law_monstrous_only }
				is_monstrous_culture = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_non_monstrous_only }
				is_non_monstrous_culture = yes
			}
			
			#Same group
			AND = {
				$COUNTRY$ = { has_law = law_type:law_giantkin_group_only }
				is_giantkin_group = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_ruinborn_group_only }
				is_ruinborn_group = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_goblinoid_group_only }
				is_goblinoid_group = yes
			}
			
			$COUNTRY$ = { has_law = law_type:law_all_races_allowed }
		}
	}
}

will_be_accepted_race = {
	OR = {
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_same_race_only }
			is_same_race_as_any_primary_culture = { # Always accept your own race
				COUNTRY = $COUNTRY$
			}
		}
		
		# Monstrous vs non-monstrous
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_monstrous_only }
			is_monstrous_culture = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_non_monstrous_only }
			is_non_monstrous_culture = yes
		}
		
		#Same group
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_giantkin_group_only }
			is_giantkin_group = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_ruinborn_group_only }
			is_ruinborn_group = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_goblinoid_group_only }
			is_goblinoid_group = yes
		}
		
		$COUNTRY$ = { is_enacting_law = law_type:law_all_races_allowed }
	}
}

#Checks if pop is accepted by CULTURAL laws
is_accepted_culture = {
    OR = {
        AND = {
            $COUNTRY$ = { has_law = law_type:law_ethnostate }
            is_primary_culture_of = $COUNTRY$
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_national_supremacy }
            AND = {
				shares_heritage_trait_with_any_primary_culture = $COUNTRY$
				shares_non_heritage_trait_with_any_primary_culture = $COUNTRY$
			}
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_racial_segregation }
            shares_heritage_trait_with_any_primary_culture = $COUNTRY$
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_cultural_exclusion }
            shares_trait_with_any_primary_culture = $COUNTRY$
        }
        $COUNTRY$ = { has_law = law_type:law_multicultural }
    }
}

culture_has_no_magical_affinity = {
	is_mechanim = yes
}

culture_has_good_magical_affinity = {
	OR = {
		culture_is_elven = yes
		is_gnomish = yes
		has_discrimination_trait = eordan
	}
}

culture_has_low_magical_affinity = {
	OR = {
		is_dwarven = yes
		is_goblin = yes
		is_orcish = yes
	}
}

pop_race_good_magical_affinity = {
	this.culture = {
		culture_has_good_magical_affinity = yes
	}
}

pop_race_low_magical_affinity = {
	this.culture = {
		culture_has_low_magical_affinity = yes
	}
}

pop_race_no_magical_affinity = {
	this.culture = {
		culture_has_no_magical_affinity = yes
	}
}