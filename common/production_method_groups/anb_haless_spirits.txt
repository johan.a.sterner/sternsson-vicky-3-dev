﻿

#pmg_high_temples_management = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_high_temple_worship
		pm_high_temple_exploration
		pm_high_temple_excavations
	}
#}
#
#pmg_spirit_presence_attitude = {
#	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
#	production_methods = {
#		pm_spirit_presence_friendly
#		pm_spirit_presence_neutral
#		pm_spirit_presence_hostile
#	}
#}