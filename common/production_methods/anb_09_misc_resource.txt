﻿## General
pm_no_enhancements = { 
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_no_treatments = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_no_automation = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

## Logging Camps
pm_summoned_blades = {
	texture = "gfx/interface/icons/production_method_icons/saw_mills.dds"
	unlocking_technologies = {
		steelworking
	}				

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 5
			
			# output goods
			goods_output_wood_add = 80
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 500
			building_employment_mages_add = 500
		}
	}
}

pm_automata_loggers = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
	
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_loggers_enforced = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
	
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -3000
		}
	}
}

pm_shredder_mechs = {
	texture = "gfx/interface/icons/production_method_icons/chainsaws.dds"
	unlocking_technologies = {
		mechs
	}
	disallowing_laws = {
		law_industry_banned
	}



	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 10	
			goods_input_engines_add = 3	#steam donkey needed 1
			goods_input_oil_add = 5	#as chainsaws use oil, plus vehicles in general
		}

		level_scaled = {
			# employment

			building_employment_laborers_add = -2000
			building_employment_machinists_add = -1000
			building_employment_engineers_add = 200
		}
	}
}

## Logging Camps - Variant - Cave Coral
pm_simple_forestry_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/simple_forestry.dds"

	building_modifiers = {
		workforce_scaled = {
			goods_output_wood_add = 20  #Default 30
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
}
pm_saw_mills_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/saw_mills.dds"
	unlocking_technologies = {
		steelworking
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_wood_add = 40 #Default 60
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}	
pm_summoned_blades_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/saw_mills.dds"
	unlocking_technologies = {
		steelworking
	}				

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 5
			
			# output goods
			goods_output_wood_add = 60 #Default 80
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 500
			building_employment_mages_add = 500
		}
	}
}
pm_electric_saw_mills_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/electric_saw_mills.dds"
	unlocking_technologies = {
		electrical_generation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_electricity_add = 5
			
			# output goods
			goods_output_wood_add = 80 #Default 100
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}

	required_input_goods = electricity
}

pm_no_hardwood_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/no_hardwood_selection.dds"
	is_default = yes
}
pm_hardwood_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/hardwood_selection.dds"
	building_modifiers = {
		workforce_scaled = {
			# output goods										
			goods_output_wood_add = -20
			goods_output_hardwood_add = 15
		}
	}
}
pm_increased_hardwood_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/increased_hardwood.dds"

	unlocking_production_methods = {
		pm_saw_mills_cave_coral
		pm_summoned_blades_cave_coral
		pm_electric_saw_mills_cave_coral
		pm_old_growth_regeneration_sawmills_cave_coral
	}

	building_modifiers = {
		workforce_scaled = {
			# output goods
			goods_output_wood_add = -40
			goods_output_hardwood_add = 30
		}
	}
}

pm_shredder_mechs_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/chainsaws.dds"
	unlocking_technologies = {
		mechs
	}
	disallowing_laws = {
		law_industry_banned
	}

	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 10	
			goods_input_engines_add = 3	#steam donkey needed 1
			goods_input_oil_add = 5	#as chainsaws use oil, plus vehicles in general
		}

		level_scaled = {
			# employment

			building_employment_laborers_add = -2000
			building_employment_machinists_add = -1000
			building_employment_engineers_add = 200
		}
	}
}

pm_automata_loggers_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
	
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_loggers_cave_coral_enforced = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
	
	unlocking_technologies = {
		early_mechanim
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	
	is_hidden_when_unavailable = yes
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -3000
		}
	}
}


## Rubber Plantation
high_velocity_irrigation_rubber_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"
	unlocking_technologies = {
		essence_extraction		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			goods_input_engines_add = 5
			
			# output goods
			goods_output_rubber_add = 75
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_farmers_add = 2000
			building_employment_machinists_add = 1000
		}
	}
}

pm_mass_cast_plant_growth_rubber_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_magical_reagents_add = 5				
			
			# output goods													
			goods_output_rubber_add = 15
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

## Fishing Wharf
pm_lightning_imbued_nets_fishing_wharf = {
	texture = "gfx/interface/icons/production_method_icons/steam_trawlers.dds"
	unlocking_technologies = {
		elemental_elicitation		
	}
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input
			goods_input_artificery_doodads_add = 5
			
			# output
			goods_output_fish_add = 20
		}

		level_scaled = {
			# employment
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = -250
			building_employment_machinists_add = 250
		}
	}
}

pm_automata_fishermen = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 8	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1500
		}
	}
}

pm_automata_fishermen_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 9	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1500
		}
	}
}

## Whaling Station
pm_no_krakeneering = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_adventurer_krakeneers = {
	texture = "gfx/interface/icons/production_method_icons/wooden_whaling_ships.dds"

	building_modifiers = {
		workforce_scaled = {
			# input
			goods_input_artillery_add = 5
			
			# output
			goods_output_magical_reagents_add = 25
			goods_output_meat_add = -5
			goods_output_oil_add = 5
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -2000
			building_employment_adventurers_add = 2500
		}
		
		unscaled = {
			building_adventurers_mortality_mult = 0.3
			building_laborers_mortality_mult = 0.2
			building_machinists_mortality_mult = 0.1
		}
	}
}
pm_formalized_krakeneer_squadrons = {
	texture = "gfx/interface/icons/production_method_icons/steam_whaling_ships.dds"

	unlocking_production_methods = {
		pm_steam_whaling_ships
	}

	building_modifiers = {
		workforce_scaled = {
			# input
			goods_input_artillery_add = 10
			goods_input_ammunition_add = 5
			
			# output
			goods_output_magical_reagents_add = 50
			goods_output_meat_add = -10
			goods_output_oil_add = 10
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -1000
			building_employment_adventurers_add = 1250
			building_employment_officers_add = 250
		}
		
		unscaled = {
			building_adventurers_mortality_mult = 0.2
			building_laborers_mortality_mult = 0.1
			building_officers_mortality_mult = 0.1
			building_machinists_mortality_mult = 0.05
		}
	}
}
pm_modern_krakeneer_fleets = {
	texture = "gfx/interface/icons/production_method_icons/steam_whaling_ships.dds"

	unlocking_production_methods = {
		pm_steam_whaling_ships
	}

	building_modifiers = {
		workforce_scaled = {
			# input
			goods_input_artillery_add = 20
			goods_input_ammunition_add = 10
			goods_input_steamers_add = 5
			goods_input_coal_add = -20
			
			# output
			goods_output_magical_reagents_add = 75
			goods_output_meat_add = -20
			goods_output_oil_add = 10
		}

		level_scaled = {
			# employment
			building_employment_engineers_add = 250
			building_employment_officers_add = 250
		}
		
		unscaled = {
			building_officers_mortality_mult = 0.05
			building_laborers_mortality_mult = 0.05
			building_machinists_mortality_mult = 0.05
		}
	}
}
pm_lightning_imbued_harpoons_whaling_station = {
	texture = "gfx/interface/icons/production_method_icons/steam_trawlers.dds"
	unlocking_technologies = {
		elemental_elicitation		
	}
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input
			goods_input_artificery_doodads_add = 5
			
			# output
			goods_output_magical_reagents_add = 5
			goods_output_magical_reagents_add = 5
			goods_output_oil_add = 5
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = -250
			building_employment_machinists_add = 250
		}
	}
}

pm_automata_whalers = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}


	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 8	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1500
		}
	}
}

pm_automata_whalers_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 9	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1500
		}
	}
}

## Oil Rigs
pm_damesoil_immersion = {
	texture = "gfx/interface/icons/production_method_icons/simple_oil_extraction.dds"
	unlocking_technologies = {
		electroarcanism_theory			
	}
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_damestear_add = 10
			
			# output goods
			goods_output_oil_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 250
			building_employment_machinists_add = 250
			building_employment_engineers_add = 250
		}
	}
}
pm_damesoil_magnetism = {
	texture = "gfx/interface/icons/production_method_icons/simple_oil_extraction.dds"
	unlocking_technologies = {
		anti_friction_magnemancy		
	}
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_damestear_add = 10
			goods_input_artificery_doodads_add = 5
			goods_input_electricity_add = 5
			
			# output goods
			goods_output_oil_add = 40
		}

		level_scaled = {
			building_employment_machinists_add = 250
			building_employment_engineers_add = 500
		}
	}
}

pm_automata_drillers = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_machinists_add = -2000
		}
	}
}

pm_automata_drillers_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7	
		}

		level_scaled = {
			building_employment_machinists_add = -2000
		}
	}
}