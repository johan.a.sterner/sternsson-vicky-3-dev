﻿goods_output_damestear_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_artificery_doodads_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_blueblood_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_relics_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_magical_reagents_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_flawless_metal_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_output_automata_add = {
	decimals = 1
	color=good
	game_data = {
		ai_value = 0
	}
}

goods_input_porcelain_add = { #anbennar, didn't exist in vanilla
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_damestear_add = { #anbennar
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_artificery_doodads_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_blueblood_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_relics_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_magical_reagents_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_flawless_metal_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_input_automata_add = {
	decimals=1
	color=bad
	game_data={
		ai_value=0
	}
}

goods_output_porcelain_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_groceries_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_magical_reagents_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_damestear_mine_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_damestear_fields_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_flawless_metal_mine_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_mage_academy_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_cave_coral_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_serpentbloom_farms_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_mushroom_farms_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_doodad_manufacturies_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_automatories_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_magical_reagents_workshop_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_tea_plantations_throughput_add = {   #somehow this isn't in vanilla????
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_damestear_mining_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_doodad_manufacturies_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

# state_pops_assembled_add = { #seperatist droid army march theme (it makes mechanim pops)
#	decimals=1
#	color=good
#	percent=no
#	game_data={
#		ai_value=0
#	}
# }

state_quest_progress_rate_add = {
	decimals=2
	color=good
	percent = no
	game_data={
		ai_value=0
	}
}

state_quest_progress_rate_mult = {
	decimals=2
	color=good
	percent = yes
	game_data={
		ai_value=0
	}
}

#country_subsidies_bg_adventurers_wanted = {
#	color=good
#	boolean = yes
#}

goods_output_damestear_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_flawless_metal_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_employment_mages_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

state_building_dwarven_hold_max_level_add = {
	color=good
	percent = no
	decimals=0
	game_data={
		ai_value=0
	}
}

goods_output_fish_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

state_pops_assembled_add = {
	decimals = 1
	color = good
	game_data={
		ai_value=0
	}
}

goods_output_iron_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_lead_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_coal_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_input_coal_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_steel_mills_throughput_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_sulfur_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

goods_output_gold_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

#Redefining these to have one decimal place instead of none

building_group_bg_plantations_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_agriculture_throughput_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

building_arts_academy_output_add = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}


