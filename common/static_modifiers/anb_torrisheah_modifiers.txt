﻿torrisheah_overstretched = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_negative.dds
    country_bureaucracy_mult = -0.3
    country_legitimacy_base_add = -20
    country_radicals_from_legitimacy_mult  = 0.2
}

torrisheah_lords = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    country_prestige_add = 10
    country_prestige_mult = 0.2
}

torrisheah_plant_invest = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    state_infrastructure_mult = 0.1
}

torrisheah_plant_invest_cost = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    country_expenses_add = 1200
}

torrisheah_plant_inaction = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_negative.dds
    state_infrastructure_mult = -0.1
    every_scope_building = {
        building_throughput_add = -0.1
    }
}

torrisheah_sabotage = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_fire_negative.dds
    building_throughput_add = -0.5
}

torrisheah_rebuilding = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
    country_expenses_add = 1200
}

torrisheah_crackdown = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
}

torrisheah_slave_revolt = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_negative.dds
    building_group_bg_plantations_throughput_add = -0.2 
    building_group_bg_agriculture_throughput_add = -0.2 
    building_group_bg_ranching_throughput_add = -0.2 
}

torrisheah_pirates_defeated = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
    country_legitimacy_base_add = 10
    country_prestige_mult = 0.05
}

torrisheah_pirates_rampaging = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_fire_negative.dds
    state_infrastructure_mult = -0.2
    state_construction_mult = -0.1
    state_standard_of_living_add = -1
}

torrisheah_privateers = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_rifle_positive.dds
    country_expenses_add = 1200
}