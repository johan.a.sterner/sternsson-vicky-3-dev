﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

ruinborn_effei = {
	template = "moon_elf"
	
	skin_color = {
		20 = { 0.0 0.5 1.0 0.52 } #Dark
	}
	
	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
	}
	hair_color = {
		# White
		5 = { 0.0 0.0 0.01 0.01 }
		# Brown
		5 = { 0.65 0.7 0.9 0.99 }
	}
	
	gene_bs_body_type = {

        20 = { name = body_fat_head_fat_low_elf   range = { 0.3 0.45 }      }
        7 = { name = body_fat_head_fat_low_elf   range = { 0.45 0.55 }     }

        20 = { name = body_fat_head_fat_medium_elf   range = { 0.3 0.45 }      }
        7 = { name = body_fat_head_fat_medium_elf   range = { 0.45 0.55 }      }

        20 = { name = body_fat_head_fat_full_elf   range = { 0.3 0.45 }      }
        7 = { name = body_fat_head_fat_full_elf   range = {0.45 0.55 }      }

	}
	
	gene_cheek_fat = {


        64 = { name = cheek_fat   range = { 0.1 0.3 }      }
        20 = { name = cheek_fat   range = { 0.3 0.5 }      }


    }

	gene_height = {

        84 = { name = normal_height   range = { 0.5 0.6 }      }

	}
}

ruinborn_laii = {
	template = "moon_elf"
	
	skin_color = {
		10 = { 0.5 0.35 0.7 0.49 }    #Brown
	}
	
	hair_color = {
		# Blonde
		5 = { 0.25 0.2 0.75 0.55 }
		# Brown
		5 = { 0.65 0.7 0.9 0.99 }
	}
	
	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
	}
	

	gene_height = {

        90 = { name = normal_height   range = { 0.6 0.8 }      }
		10 = { name = normal_height   range = { 0.7 0.9 }      }

	}
}

ruinborn_paura = {
	template = "ruinborn_effei"
	
	skin_color = {
		10 = { 0.5 0.25 0.7 0.40 }    #Lighter Brown
	}
	
	hair_color = {	
		# Blue to Turquoise
		95 = { 1.0 0.08 1.0 0.16 }
	}
}

ruinborn_thamvoi = {
	template = "ruinborn_effei"
	
	skin_color = {
		10 = { 0.5 0.35 0.7 0.49 }    #Brown
	}
	
	hair_color = {	
		# Green
		95 = { 1.0 0.34 1.0 0.49 }
	}
}

ruinborn_oronoi = {
	template = "moon_elf"
	
	skin_color = {
		10 = { 0.5 0.35 0.7 0.49 }    #Brown
	}
	
	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
	}
	
	hair_color = {
		# White
		5 = { 0.0 0.0 0.01 0.01 }
	}
	
	gene_bs_body_type = {

        5 = { name = body_fat_head_fat_low_elf   range = { 0.45 0.55 }     }
		5 = { name = body_fat_head_fat_low_elf   range = { 0.55 0.7 }      }

        5 = { name = body_fat_head_fat_medium_elf   range = { 0.45 0.55 }      }
		5 = { name = body_fat_head_fat_medium_elf   range = { 0.55 0.7 }      }

        5 = { name = body_fat_head_fat_full_elf   range = {0.45 0.55 }      }
		5 = { name = body_fat_head_fat_full_elf   range = { 0.55 0.7 }      }

	}
	
	gene_cheek_fat = {


        50 = { name = cheek_fat   range = { 0.3 0.5 }      }
		50 = { name = cheek_fat   range = { 0.5 0.6 }      }


    }
}


ruinborn_seedthrall = {
	template = "ruinborn_effei"
	
	skin_color = {
		10 = { 0.4 0.51 0.45 0.55 }    #green
	}
	
	hairstyles = {
		20 = { name = no_hairstyles 		range = { 0.0 1.0 } }
	}
}
