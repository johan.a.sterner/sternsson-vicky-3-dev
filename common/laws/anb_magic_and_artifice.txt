﻿law_artifice_banned = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.75
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { initialize_spell_list = yes }
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = -200
	
}

law_nation_of_magic = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.5
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { initialize_spell_list = yes }
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = -100
}

law_nation_of_green_artifice = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_magical_expertise_decree_cost_mult = -0.25
		country_tech_research_speed_mult = 0.025
		state_pollution_reduction_health_mult = 1.0
	}
	
	is_visible = {
		has_variable = green_artifice_unlocked
	}
	
	can_enact = {
		has_variable = green_artifice_unlocked
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { initialize_spell_list = yes }
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 100
}

law_nation_of_artifice = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	disallowing_laws = {
		law_magocracy
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = { initialize_spell_list = yes }
	}
	
	modifier = {
		country_tech_research_speed_mult = 0.05
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 100
	
}

law_traditional_magic_banned = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	disallowing_laws = {
		law_magocracy
	}
	
	
	modifier = {
		country_tech_research_speed_mult = 0.1
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		hidden_effect = {
			initialize_spell_list = yes 
			remove_variable = military_magic_spell
			remove_variable = military_magic_spell_level
			remove_variable = military_magic_targeted_spell_level
			remove_variable = military_magic_spell_level_floor
			remove_variable = military_magic_spell_changing_level
			update_military_spell = yes
			remove_variable = society_magic_spell
			remove_variable = society_magic_spell_level
			remove_variable = society_magic_targeted_spell_level
			remove_variable = society_magic_spell_level_floor
			remove_variable = society_magic_spell_changing_level
			update_society_spell = yes
			remove_variable = production_magic_spell
			remove_variable = production_magic_spell_level
			remove_variable = production_magic_targeted_spell_level
			remove_variable = production_magic_spell_level_floor
			remove_variable = production_magic_spell_changing_level
			update_production_spell = yes
		}
	}

	ai_enact_weight_modifier = {
		value = 0
	}

	progressiveness = 200
}