﻿#For realizing the dream
je_equality_of_races = {
	icon = "gfx/interface/icons/event_icons/event_scales.dds"

	complete = {
		has_law = law_type:law_all_races_allowed
	}
	
	is_progressing = {
		is_enacting_law = law_type:law_all_races_allowed
	}

	weight = 1001
	transferable = yes
}